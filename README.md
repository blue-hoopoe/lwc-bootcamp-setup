# Installation

Open these 3 commands:

1. git clone https://blue-hoopoe@bitbucket.org/blue-hoopoe/lwc-bootcamp-setup.git *(bash)*
2. SFDX: Authorize an Org *(VSCode)*
3. node setup.js *(bash)*

After that your Org is ready for development! 🎉