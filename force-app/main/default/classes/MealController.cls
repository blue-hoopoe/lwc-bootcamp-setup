public with sharing class MealController {
    
    @AuraEnabled(Cacheable=true)
    public static Meal__c[] getAllMeals() {
        return [SELECT Id, Name__c, Price__c, Category__c, Image__c FROM Meal__c];
    }

    @AuraEnabled
    public static Map<String, Object> createOrder(Order__c order, Order_Item__c[] items) {
        insert order;
        for (Order_Item__c orderItem : items) {
            orderItem.Order__c = order.Id;
        }
        insert items;

        Map<String, Object> response =  new Map<String, Object>();
        response.put('position', Integer.valueof((Math.random() * 10) + 1));
        response.put('estimation', Integer.valueOf(Math.Round(Math.Random() * (260-30) + 30)));
        return response;
    }
}