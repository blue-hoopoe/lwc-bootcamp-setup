const exec = require(`child_process`).exec;
const path = require(`path`);
const fs = require(`fs`);

const base = path.resolve(__dirname, `force-app/main/default`);
const errorBreak = (message) => {
    throw new Error(message || `Error executing SFDX command. Ask Dominik or Maciej for help ;)`);
}

const asyncExec = (command) => {
    return new Promise((resolve, reject) => {
        exec(command, (error) => {
            if (error) {
                reject(arguments);
            } else {
                resolve(arguments);
            }
        })
    });
}

const deploy = (target, humanName) => {
    return new Promise((resolve, reject) => {
        console.log(`Deploying: ${humanName}...`);
        asyncExec(`sfdx force:source:deploy --sourcepath "${path.resolve(base, target)}" --json --loglevel fatal`)
            .then(() => {
                console.log(`Deployed sucesfully.`);
                resolve(arguments);
            })
            .catch(reject);
    });
}

if (!fs.existsSync(base)) {
    errorBreak(`Didn't find a force-app folder with required data.`);
}

// Add custom objects to Org.
deploy(`objects`, `Custom Objects`).then(() => {
    return deploy(`tabs`, `Tabs`);
}).then(() => {
    return deploy(`flexipages`, `Flexi Pages`);
}).then(() => {
    return deploy(`contentassets`, `Content Assets`);
}).then(() => {
    return deploy(`applications`, `Application`);
}).then(() => {
    return deploy(``, `Apex classes, Aura and others`);
}).then(() => {
    console.log(`Setting up permissions...`);
    asyncExec(`sfdx force:user:permset:assign -n McKiosk_admin`);
}).then(() => {
    console.log(`Importing example data...`);
    asyncExec(`sfdx force:data:tree:import --plan "${path.resolve(__dirname, `data/sample-data-plan.json`)}"`);
}).catch(() => {
    errorBreak();
})